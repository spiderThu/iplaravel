<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

/*Design UI*/
Route::get('/', 'DesignController@index')->name('index');
Route::get('services', 'DesignController@services')->name('services');
Route::get('projects', 'DesignController@projects')->name('projects');
Route::get('aboutus', 'DesignController@aboutus')->name('aboutus');
Route::get('contactus', 'DesignController@contactus')->name('contactus');
// Route::get('get-video/{video}', 'DesignController@getVideo')->name('getVideo');
Route::post('send', 'DesignController@send')->name('send');

/*admin dashboard*/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// Route::get('login', 'HomeController@adminLogin')->name('login');
// Route::get('admin-login', 'HomeController@adminLogin')->name('admin-login');
// Route::post('/save-login', 'HomeController@saveLogin')->name('save-login');
Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});
Route::get('service-list', 'DashboardController@serviceList')->name('service-list');
Route::get('edit-service', 'DashboardController@editService')->name('edit-service');
Route::get('addnew-service', 'DashboardController@addNewService')->name('addnew-service');
Route::get('project-list', 'DashboardController@projectList')->name('project-list');
Route::get('edit-project', 'DashboardController@editProject')->name('edit-project');
Route::get('addnew-project', 'DashboardController@addNewProject')->name('addnew-project');
Route::get('activity-list', 'DashboardController@activityList')->name('activity-list');
Route::get('edit-activity', 'DashboardController@editActivity')->name('edit-activity');
Route::get('addnew-activity', 'DashboardController@addNewActivity')->name('addnew-activity');
Route::get('admin-list', 'DashboardController@adminList')->name('admin-list');
Route::get('edit-admin/{id}', 'DashboardController@editAdmin')->name('edit-admin');
Route::post('/update-admin/{id}', 'DashboardController@updateAdmin')->name('update-admin');
Route::get('admin-register', 'DashboardController@adminRegister')->name('admin-register');
Route::post('save-admin', 'DashboardController@saveAdmin')->name('save-admin');


