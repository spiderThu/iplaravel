<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use View;
use Mail;

class DesignController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }
    public function services()
    {
        return view('services');
    }
    public function projects()
    {
        return view('projects');
    }
    public function aboutus()
    {   
        $video = "storage/ipdesign.mp4";
        $mime = "storage/mp4";
        $title = "Os Simpsons";
        return view('aboutus')->with(compact('video', 'mime', 'title'));
        //return view('aboutus');
    }
//     public function getVideo(Video $video)
// {
//     $name = $video->name;
//     $fileContents = Storage::disk('local')->get("uploads/videos/{$name}");
//     $response = Response::make($fileContents, 200);
//     $response->header('Content-Type', "video/mp4");
//     return $response;
// }
    public function contactus()
    {
        return view('contactus');
    }
    public function send(Request $request)
    {
        $mailid = $request->email;
        $name = $request->name;
        $sub = $request->subject;
        $phone = $request->phone;
        $msg = $request->msg;
        $subject = 'IP Customer Information';
    
        $data = array('email' => $mailid, 'subject' => $subject, 
                      'name' => $name, 
                      'sub' => $sub, 
                      'phone' => $phone, 
                      'msg' => $msg, 
                        );

        Mail::send('emails.newsinfo', $data , function ($message) use ($data) {
            $message->from('info@spidernetworkict.com', 'IP Customer Information');
            $message->to($data['email']);
            $message->subject($data['subject']); 
        });

        return redirect()->back()->with('success', ['Your message had been send successfully!']);   
    }
}
