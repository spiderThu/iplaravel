<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use \Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    /*service Management*/
    public function serviceList()
    {
        return view('service_list');
    }
    public function editService()
    {
        return view('edit_service');
    }
    public function addNewService()
    {
        return view('addnew_service');
    }
    /*project Management*/
    public function projectList()
    {
        return view('project_list');
    }
    public function editProject()
    {
        return view('edit_project');
    }
    public function addNewProject()
    {
        return view('addnew_project');
    }
    /*activity Management*/
    public function activityList()
    {
        return view('activity_list');
    }
    public function editActivity()
    {
        return view('edit_activity');
    }
    public function addNewActivity()
    {
        return view('addnew_activity');
    }
    /*admin Management*/
    public function adminList()
    {   
        $data = DB::table('users')->get();
        $auth = new User();
        $test = $auth->getPasswordAttribute('thuthuaung');
        //dd($test);exit();
        return view('admin_list', compact('data'));
    }
    public function editAdmin($id)
    {   
        $users = User::findOrFail($id);
        return view('edit_admin',compact('users'));
    }
    public function updateAdmin(Request $request, $id)
    {   
        $users = User::findOrFail($id);

        $new = $request->password;
        $confirmed = $request->new_password;

        if ($new === $confirmed) { 
            $users->name = request('name');
            $users->email = request('email');
            $users->password =  Hash::make(request('password'));
            $users->save();

            $request->session()->flash('success', 'Password changed');
            return redirect()->route('admin-list')->with('success', 'Admin Info Updated!');

        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
        // return redirect('/admin-list');
    }
    public function adminRegister()
    {
        return view('admin_register');
    }
    public function saveAdmin(Request $request){
        request()->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        ]);
         
        $data = $request->all();
 
        $check = $this->create($data);
       
        return Redirect::to("home")->withSuccess('Great! You have Successfully loggedin');
    }
    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' =>  Hash::make($data['password'])
      ]);
    }

}
