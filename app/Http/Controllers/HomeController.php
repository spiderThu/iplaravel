<?php

namespace App\Http\Controllers;
use \Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    // public function adminLogin(){
    //    return view('auth.login');
    // }
    // public function saveLogin(Request $request){
    //    //validate the fields....
    //    $password =  Crypt::encrypt($request->password);
    //    dd($password);exit();
    //    $credentials = [ 'email' => $request->email , 'password' => $request->password ];
  
    //     if(Auth::attempt($credentials,$request->remember)){ // login attempt
    //         //login successful, redirect the user to your preferred url/route...
    //         return redirect('/home');
    //     }
  
    //     //login failed...
    //     return Redirect::back ();
    // }

}
