<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Crypt;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getPasswordAttribute($value)
    {
        if( \Hash::needsRehash($value) ) {
                $value = \Hash::make($value);
        }

        return  $value;
    }

    // public function getNameAttribute($value) {
    //     return Crypt::decryptString($value);
    // }

    // public function getEmailAttribute($value) {
    //     return Crypt::decryptString($value);
    // }

    // public function getPasswordAttribute($value) {
    //     return Crypt::decryptString($value);
    // }

    // public function setNameAttribute($value) {
    //     $this->attributes['name'] = Crypt::encryptString($value);
    // }

    // public function setEmailAttribute($value) {
    //     $this->attributes['email'] = Crypt::encryptString($value);
    // }

    // public function setPasswordAttribute($value) {
    //     $this->attributes['password'] = Crypt::encryptString($value);
    // }
}
