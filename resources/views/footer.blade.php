<!-- Footer -->
<footer class="footer-section">
   <div class="container-fluid">
      <div class="row">
         <div class="footer-left-agileinfo col-xl-5">
            <iframe class="facebook" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fipcctvinstaller%2F&tabs=timeline&width=480&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
         </div>
         <div class="footer-right-w3ls col-xl-7 p-sm-5 p-4">
            <div class="row">
               <div class="col-lg-6 footer-grids-w3layouts">
                  <h2>Our Projects</h2>
                  <div class="container-fluid">
                     <div class="row">
                        <a href="#" class="col-4">
                        <img src="images/ft1.jpg" class="img-fluid" alt="Responsive image">
                        </a>
                        <a href="#" class="col-4">
                        <img src="images/ft2.jpg" class="img-fluid" alt="Responsive image">
                        </a>
                        <a href="#" class="col-4">
                        <img src="images/ft3.jpg" class="img-fluid" alt="Responsive image">
                        </a>
                     </div>
                     <div class="row">
                        <a href="#" class="col-4">
                        <img src="images/ft4.jpg" class="img-fluid" alt="Responsive image">
                        </a>
                        <a href="#" class="col-4">
                        <img src="images/ft5.jpg" class="img-fluid" alt="Responsive image">
                        </a>
                        <a href="#" class="col-4">
                        <img src="images/ft6.jpg" class="img-fluid" alt="Responsive image">
                        </a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 footer-grids-w3layouts mt-xl-0 mt-4">
                  <h3>Get in touch</h3>
                  <address>
                     <p>No.001, Ground Floor, Building 6, <br>
                        Aung Chan Thar Housing, Tamwe Tsp, <br>
                        Near OCEAN Supermarket
                     </p>
                     <p>+95-09795099956,</p>
                     <p>+95-09965099956</p>
                     <p>Mail:
                        <a href="mailto:info@intellpower.com">info@intellpower.com</a>
                     </p>
                  </address>
               </div>
               <div class="col-lg-6 footer-grids-w3layouts mt-xl-5 mt-4">
                  <h3>Links</h3>
                  <ul class="w3agile_footer_grid_list">
                     <li>
                        <a href="{{ route('index') }}">Home</a>
                     </li>
                     <li>
                        <a href="{{ route('services') }}">Services</a>
                     </li>
                     <li>
                        <a href="{{ route('projects') }}">Projects</a>
                     </li>
                     <li>
                        <a href="{{ route('aboutus') }}">About Us</a>
                     </li>
                     <li>
                        <a href="{{ route('contactus') }}">Contact Us</a>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 footer-grids-w3layouts mt-xl-5 mt-4">
                  <h3>Social Media</h3>
                  <ul class="social_list_wthree">
                     <li><iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fipcctvinstaller%2F&width=140&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId" width="140" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>
<!-- //Footer -->
<!-- copyright -->
<div class="copyright-w3layouts">
   <div class="container">
      <p class="py-xl-4 py-3">© 2019 Intelligence Power Co,Ltd. All Rights Reserved | Design by
         <a href="https://ihost.spidernetworkict.com/" target="blank_"> iHost </a>
      </p>
   </div>
</div>
<!-- //copyright -->

