<!--
Author: iHost
Author URL: http://ihost.spidernetworkict.com/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Intelligence Power Company Limited</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="ip,intellpower,Intelligence,cctv,ipcctv,ippower,network,IntelligencePower" />
<LINK rel="SHORTCUT ICON" href="images/ipshortcut.png">

<!-- css files -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
<link href="css/loginstyle.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css files -->

<!-- online fonts -->
<link href="//fonts.googleapis.com/css?family=Sirin+Stencil" rel="stylesheet">
<!-- online fonts -->

<body>
<div class="container demo-1">
    <div class="content">
        <div id="large-header" class="large-header">
            <a href="{{ route('index') }}">
            <h1><img src="images/ipshortcut.png" style="width: 40px;"> Intelligence Power Co.Ltd</h1></a>
                <div class="main-agileits">
                <!--form-stars-here-->
                        <div class="form-w3-agile">
                            <h2>Admin Login</h2>
                            <form method="POST" action="{{ route('login') }}">
                            @csrf
                                <div class="form-sub-w3">
                                    <input type="text" name="email" placeholder="Email" id="email" required="" />
                                <div class="icon-w3">
                                    📧
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: #fff;">{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="form-sub-w3">
                                    <input type="password" name="password" placeholder="Password" id="password" required="" />
                                <div class="icon-w3">
                                    🔍
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="clear"></div>
                                <div class="submit-w3l">
                                    <input type="submit" value="Login">
                                </div>
                            </form>
                        </div>
                <!--//form-ends-here-->
                </div><!-- copyright -->
                    <div class="copyright w3-agile">
                        <p> © 2019 Intelligence Power Co,Ltd. All rights reserved | Design by <a href="https://ihost.spidernetworkict.com/" target="blank_"> iHost </a></p>
                    </div>
                    <!-- //copyright --> 
        </div>
    </div>
</div>  

</body>
</html>