@include('header')

	@if (Session::has('success'))
      <script type="text/javascript"> 
         alert("Your message had been send successfully!");
      </script>
    @endif
	<!-- breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{{ route('index') }}">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Contact</li>
		</ol>
	</nav>
	<!-- //breadcrumb -->
	<!-- contact -->
	<section class="contact py-5">
		<div class="container py-xl-5 py-sm-3">
			<h5 class="main-w3l-title mb-sm-4 mb-3">Contact Us</h5>
			<div class="wthree_contact_left">
				<h3 class="subheading-wthree mb-md-4 mb-3">Send us an Email</h3>
				<form action="{{url('send')}}" method="post">
					@csrf
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Name</label>
							<input type="text" name="name" class="form-control" placeholder="Name" required="">
						</div>
						<div class="form-group col-md-6">
							<label>Phone</label>
							<input type="text" name="phone" class="form-control" placeholder="phone" required="">
						</div>
						<div class="form-group col-md-6">
							<label for="exampleFormControlInput1">Email</label>
							<input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Email" required="">
						</div>
						<div class="form-group col-md-6">
							<label>Subject</label>
							<input type="text" name="subject" class="form-control" placeholder="Subject" required="">
						</div>
					</div>
					<div class="form-group">
						<label for="textarea">Message</label>
						<textarea id="textarea" name="msg" placeholder="Message..." required=""></textarea>
					</div>
					<button type="submit" class="btn btn-primary py-sm-3 py-2 px-5">Submit</button>
				</form>
			</div>
		</div>
	</section>
	<div class="map-agileits">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30554.50089127625!2d96.14754924520318!3d16.810835159283172!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c1ecb6de37e039%3A0x2137fbdee8f444ff!2sIntelligence%20Power%20Co.%2C%20Ltd.!5e0!3m2!1sen!2smm!4v1574235162524!5m2!1sen!2smm" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	</div>
	<!-- //contact -->

	@include('footer')
	
	<!-- Required common Js -->
	<script src='js/jquery-2.2.3.min.js'></script>
	<!-- //Required common Js -->

	<!-- password-script -->
	<script>
		window.onload = function () {
			document.getElementById("password1").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}

		function validatePassword() {
			var pass2 = document.getElementById("password2").value;
			var pass1 = document.getElementById("password1").value;
			if (pass1 != pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');
			//empty string means no validation error
		}
	</script>
	<!-- //password-script -->

	<!-- start-smoth-scrolling -->
	<script src="js/move-top.js"></script>
	<script src="js/easing.js"></script>
	<script>
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<!-- here stars scrolling icon -->
	<script>
		$(document).ready(function () {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- //here ends scrolling icon -->
	<!--js for bootstrap working-->
	<script src="js/bootstrap.min.js"></script>
	<!-- //for bootstrap working -->
</body>

</html>