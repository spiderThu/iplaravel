@include('header')
<!-- breadcrumb -->
<nav aria-label="breadcrumb">
   <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{{ route('index') }}">Home</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">About Us</li>
   </ol>
</nav>
<!-- //breadcrumb -->
<!--about-->
<section class="about-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">About Us</h5>
      <p class="paragraph-agileinfo">Intelligence Power Co., Ltd.(IP) was legally founded in 2016. We believe that there is a need for security and Hotel System (ELV) in Hotel, Guest House ,Residential, Business locations, Government Institutes and Retail Shops.With increasing rate of crime over our environment, it is very critical to have safe and reliable security system in your organisation. It is even more important to have security systems than cannot easily be tempered with and can be accessible and monitored remotely at any location, at any time in the globe. Intelligence Power Co., Ltd.(IP) offers the best security services to meet the needs of your organisation. We are accredited installers for the highest quality, tested and reliable products from the best national and international security systems suppliers. Our company is successfully giving a perfect
service with variety of branded products , latest technology and software for ELV system solution.
      </p>
      <!-- <video class="videoipdesign" controls>
         <source src="{{URL::asset('ipdesign.mp4')}}" type="video/mp4">
         <source src="{{URL::asset('ipdesign.webm')}}" type="video/webm" />
      </video> -->

      <!-- <video width="320" height="240" controls>  
       <source src="{{URL::asset('ipdesign.mp4')}}" type="video/mp4">  
       <source src="{{URL::asset('ipdesign.webm')}}" type="video/webm">  
       Your browser does not support the video tag.
      </video> -->

      <p class="paragraph-agileinfo">We offer high quality security services/products from well-known brands at competitive prices. Our responsibility in the projects , we allow our clients to give 1 year warranty. For further
maintenance , we provide 24 hours on call service with reasonable charges.</p>
      <br>
      <div class="row mt-xl-5 mt-4">
         <div class="col-md-6 bb1 about-left p-md-5 p-4">
            <h3 class="subheading-wthree mb-md-4 mb-3">Company Details</h3>
            <table>
               <tr style="font-size: 18px;">
                  <td>Business Name:</td>
                  <td><span style="color: #fff;font-weight: 600;">Intelligence Power Co., Ltd.(IP)</span></td>
               </tr>
               <tr style="font-size: 18px;">
                  <td>Company Registration Number:</td>
                  <td><span style="color: #fff;font-weight: 600;">2016/109489069/11</span></td>
               </tr>
               <tr style="font-size: 18px;">
                  <td>Shareholders:</td>
                  <td><span style="color: #fff;font-weight: 600;">100% Private Company Limited by Shares</span></td>
               </tr>
            </table>
            <!-- <p class="paragraph-agileinfo text-white"> <span style="color: #010986;font-weight: 600;"> </span><br>
 <span style="color: #010986;font-weight: 600;"> </span><br>
 <span style="color: #010986;font-weight: 600;"></span>
            </p> -->
         </div>
         <div class="col-md-6 bb2 about-left p-md-5 p-4">
            <h3 class="subheading-wthree mb-md-4 mb-3">IP Technicians</h3>
            <p class="paragraph-agileinfo text-white">Intelligence Power Co., Ltd.(IP) is an accredited installer of many brands with many
years of experience in the electronic security systems industry. We have hired highly qualified technicians to perform our quality installations, with the goal to surpass our customer’s expectations. 
            </p>
         </div>
      </div>
   </div>
</section>
<!--//about-->
<!-- News -->
<h5 class="main-w3l-title mb-sm-4 mb-3">Our Activities</h5>
<section class="Single-page py-5">
   <div class="container py-xl-5 py-sm-3">
      <div class="row">
         <!--left-->
         <div class="col-lg-8 left-blog-info text-left">
            <div class="w3_agile_services_grid my-md-0 my-4">
               <div class="agile_services_grid">
                  <div class="hover06 column">
                     <div>
                        <a href="#">
                        <img src="images/certificate.jpg" class="img-fluid" alt="Responsive image">
                        </a>
                     </div>
                  </div>
                  <div class="agile_services_grid_pos">
                     <span class="py-2 px-3">14 November</span>
                  </div>
               </div>
               <h4 class="mt-3 mb-2">
                  <a href="#">Certificate Of Authorization</a>
               </h4>
               <p class="paragraph-agileinfo">We hereby authorize Intelligence Power Co,Ltd. as authorized distributor
                  to sell all the audio products under the brand of PEAa.
               </p>
            </div>
         </div>
         <!--//left-->
         <!--right-->
         <aside class="col-lg-4 mt-lg-0 mt-5 right-blog-con text-right">
            <div class="right-blog-info text-left">
               <div class="tech-btm">
                  <img src="images/banner1.jpg" class="img-fluid" alt="">
               </div>
               <div class="tech-btm">
                  <h4 class="mb-md-4 mb-3">Recent Posts Of Intelligence Power Co,ltd.</h4>
                  <div class="blog-grids row mb-3">
                     <div class="col-md-5 blog-grid-left">
                        <a href="#">
                        <img src="images/cctv1.jpg" class="img-fluid" alt="">
                        </a>
                     </div>
                     <div class="col-md-7 blog-grid-right">
                        <h5>
                           <a href="#">ပါရမီရှိ တိုက်နှစ်လုံး တူရိယာဆိုင်တွင် IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည။ </a>
                        </h5>
                        <div class="sub-meta">
                           <span>
                           💭 19 Augest, 2019 </span>
                        </div>
                     </div>
                  </div>
                  <div class="blog-grids row mb-3">
                     <div class="col-md-5 blog-grid-left">
                        <a href="#">
                        <img src="images/cctv2.jpg" class="img-fluid" alt="">
                        </a>
                     </div>
                     <div class="col-md-7 blog-grid-right">
                        <h5>
                           <a href="#"> GWAMBO STUDIOS တွင် Uniview IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။ </a>
                        </h5>
                        <div class="sub-meta">
                           <span>
                           💭 19 Augest, 2019 </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </aside>
         <!--//right-->
      </div>
   </div>
</section>
<!-- //News -->
<!-- Team -->
<section class="team-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">About Our Team</h5>
      <div class="row">
         <div class="col-lg-3 col-md-6 grid_info">
            <img src="images/mr.jpg" class="img-fluid" alt="Responsive image">
            <div class="team_info p-3">
               <h5>Mr.Htun Htun Aung</h5>
               <span class="mt-1 mb-2">Director</span>
               <ul class="social_list_wthree">
                  <li>
                     <a href="https://www.facebook.com/ipcctvinstaller/" target="blank_" class="facebook1 text-center">
                     <img src="images/fbicon.png">
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="col-lg-3 col-md-6 grid_info second">
            <img src="images/mr.jpg" class="img-fluid" alt="Responsive image">
            <div class="team_info p-3">
               <h5>Mr.Htun Htun Aung</h5>
               <span class="mt-1 mb-2">CEO</span>
               <ul class="social_list_wthree">
                  <li>
                     <a href="#" class="facebook1 text-center">
                     <img src="images/fbicon.png">
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="col-lg-3 col-md-6 grid_info">
            <img src="images/mr.jpg" class="img-fluid" alt="Responsive image">
            <div class="team_info p-3">
               <h5>Mr.Htun Htun Aung</h5>
               <span class="mt-1 mb-2">Project Manager</span>
               <ul class="social_list_wthree">
                  <li>
                     <a href="https://www.facebook.com/ipcctvinstaller/" target="blank_" class="facebook1 text-center">
                     <img src="images/fbicon.png">
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="col-lg-3 col-md-6 grid_info second">
            <img src="images/mr.jpg" class="img-fluid" alt="Responsive image">
            <div class="team_info p-3">
               <h5>Mr.Htun Htun Aung</h5>
               <span class="mt-1 mb-2">HR Manager</span>
               <ul class="social_list_wthree">
                  <li>
                     <a href="https://www.facebook.com/ipcctvinstaller/" target="blank_"  class="facebook1 text-center">
                     <img src="images/fbicon.png">
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<!--// Team -->
@include('footer')
<!-- Appointment Modal -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Make an Appointment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="login p-md-5 p-4">
               <form action="#" method="post">
                  <div class="form-group">
                     <label>Name</label>
                     <input type="text" class="form-control" placeholder="" required="">
                  </div>
                  <div class="form-group">
                     <label class=" mb-2">Email</label>
                     <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="" required="">
                  </div>
                  <div class="form-group">
                     <label>Phone</label>
                     <input type="text" class="form-control" placeholder="" required="">
                  </div>
                  <div class="form-group">
                     <label>Message</label>
                     <textarea id="textarea" placeholder=""></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary submit">Submit</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!--// Appointment Modal -->
<!-- Required common Js -->
<script src='js/jquery-2.2.3.min.js'></script>
<!-- //Required common Js -->
<!-- stats -->
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.countup.js"></script>
<script>
   $('.counter').countUp();
</script>
<!-- //stats -->
<!-- password-script -->
<script>
   window.onload = function () {
   	document.getElementById("password1").onchange = validatePassword;
   	document.getElementById("password2").onchange = validatePassword;
   }
   
   function validatePassword() {
   	var pass2 = document.getElementById("password2").value;
   	var pass1 = document.getElementById("password1").value;
   	if (pass1 != pass2)
   		document.getElementById("password2").setCustomValidity("Passwords Don't Match");
   	else
   		document.getElementById("password2").setCustomValidity('');
   	//empty string means no validation error
   }
</script>
<!-- //password-script -->
<!-- start-smoth-scrolling -->
<script src="js/move-top.js"></script>
<script src="js/easing.js"></script>
<script>
   jQuery(document).ready(function ($) {
   	$(".scroll").click(function (event) {
   		event.preventDefault();
   		$('html,body').animate({
   			scrollTop: $(this.hash).offset().top
   		}, 1000);
   	});
   });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script>
   $(document).ready(function () {
   	/*
   		var defaults = {
   		containerID: 'toTop', // fading element id
   		containerHoverID: 'toTopHover', // fading element hover id
   		scrollSpeed: 1200,
   		easingType: 'linear' 
   		};
   	*/
   
   	$().UItoTop({
   		easingType: 'easeOutQuart'
   	});
   
   });
</script>
<!-- //here ends scrolling icon -->
<!--js for bootstrap working-->
<script src="js/bootstrap.min.js"></script>
<!-- //for bootstrap working -->
</body>
</html>

