<!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Intelligence Power Company Limited</title>
      <!-- Meta Tags -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="utf-8">
      <meta name="keywords" content="ip,intellpower,Intelligence,cctv,ipcctv,ippower,network,IntelligencePower" />
      <LINK rel="SHORTCUT ICON" href="images/ipshortcut.png">
      <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script>
      <!-- //Meta Tags -->
      <!-- Style-sheets -->
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
      <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
      <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
      <link href="css/fontawesome-all.css" rel="stylesheet">
      <!--// Style-sheets -->
      <!--web-fonts-->
      <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
      <!--//web-fonts-->
   </head>
   <body>
      <!-- banner -->
      <section class="top-agileits-w3layouts container-fluid p-3">
         <div class="row">
            <h1 class="col-md-4 logo order-md-2 order-1 text-center" >
               <a class="navbar-brand" href="{{ route('index') }}">
               <i class="fab fa-keycdn"><img src="images/ipshortcut.png" style="width: 17%;">
               Intelligence Power <span style="font-size:20px;color:#00b9f1;">Co.Ltd</span>
               </i>
               </a>
            </h1>
            <div class="col-md-4 order-md-1 order-2 text-left align-self-center top-mid-w3l">
               <!-- Login Button -->
               <a href="{{ route('login') }}">
               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
               Login
               </button></a>
               <!--// Login Button -->
            </div>
            <div class="col-md-4 order-3 log-icons text-right align-self-center">
               <ul class="social_list_wthree">
                  <li class="text-center">
                     <a href="#">
                     <i><img src="images/usa.png" style="width:120%;"></i>
                     </a>
                  </li>
                  <li class="text-center mx-3">
                     <a href="#">
                     <i><img src="images/myanmar.png" style="width:120%;"></i>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </section>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'services'){
            echo "<section class='banner b1 inner-banner' id='home'>";
         }
      ?>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'projects'){
            echo "<section class='banner b1 inner-banner' id='home'>";
         }
      ?>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'aboutus'){
            echo "<section class='banner b1 inner-banner' id='home'>";
         }
      ?>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'contactus'){
            echo "<section class='banner b1 inner-banner' id='home'>";
         }
      ?>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'index'){
            echo "<section class='banner' id='home'>";
         }
      ?>
      <!-- header -->
      <header>
         <nav class="navbar navbar-expand-lg navbar-light bg-light top-header-w3layouts">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
               aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
                     <?php 
                     $routeName = Request::route()->getName();
                     if($routeName == 'index'){
                        echo "<li class='nav-item active'>";
                     }
                     ?>
                     <a class="nav-link ml-lg-0" href="{{ route('index') }}">Home
                     </a>
                  </li>
                     <?php 
                     $routeName = Request::route()->getName();
                     if($routeName == 'services'){
                        echo "<li class='nav-item active'>";
                     }
                     ?>
                     <a class="nav-link" href="{{ route('services') }}">Services
                     </a>
                  </li>
                  <?php 
                     $routeName = Request::route()->getName();
                     if($routeName == 'projects'){
                        echo "<li class='nav-item active'>";
                     }
                     ?>
                     <a class="nav-link" href="{{ route('projects') }}">Projects</a>
                  </li>
                  <?php 
                     $routeName = Request::route()->getName();
                     if($routeName == 'aboutus'){
                        echo "<li class='nav-item active'>";
                     }
                     ?>
                     <a class="nav-link" href="{{ route('aboutus') }}">About Us
                     </a>
                  </li>
                  <?php 
                     $routeName = Request::route()->getName();
                     if($routeName == 'contactus'){
                        echo "<li class='nav-item active'>";
                     }
                     ?>
                     <a class="nav-link" href="{{ route('contactus') }}">Contact Us</a>
                  </li>
               </ul>
               <div class="col-md-4 order-3 log-icons text-right align-self-center">
                  <ul class="social_list_wthree">
                     <li>
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fipcctvinstaller%2F&width=140&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId" width="140" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                     </li>
                     <!-- <li class="text-center">
                        <a href="#">
                        <i><img src="images/flike.png" style="width:200%;"></i>
                        </a>
                     </li>
                     &nbsp;	&nbsp; 	&nbsp;
                     <li class="text-center mx-3">
                        <a href="#">
                        <i><img src="images/fshare.png" style="width:200%;"></i>
                        </a>
                     </li> -->
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <!-- //header -->
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'services'){
            echo "<div class='container'>";
            echo " <h3 class='inner-title'>Services</h3>";
            echo " </div>";
         }
      ?>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'projects'){
            echo "<div class='container'>";
            echo " <h3 class='inner-title'>Projects</h3>";
            echo " </div>";
         }
      ?>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'aboutus'){
            echo "<div class='container'>";
            echo " <h3 class='inner-title'>About</h3>";
            echo " </div>";
         }
      ?>
      <?php 
         $routeName = Request::route()->getName();
         if($routeName == 'contactus'){
            echo "<div class='container'>";
            echo " <h3 class='inner-title'>Contact</h3>";
            echo " </div>";
         }
      ?>
      
        
     
   </section>

