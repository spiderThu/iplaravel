@include('header')
<!-- banner-text -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
   <div class="carousel-inner text-right">
      <div class="carousel-item b1 active">
         <div class="container banner-info-w3-agile">
            <h3>Intelligence Power Co,Ltd.</h3>
            <p class="mt-lg-4 mt-md-3 mt-2 p-2 pl-4">CCTV & Security Solutions...</p>
         </div>
      </div>
      <div class="carousel-item b2">
         <div class="container banner-info-w3-agile">
            <h3>Intelligence Power Co,Ltd.</h3>
            <p class="mt-lg-4 mt-md-3 mt-2 p-2 pl-4">CCTV & Security Solutions...</p>
         </div>
      </div>
      <div class="carousel-item b3">
         <div class="container banner-info-w3-agile">
            <h3>Intelligence Power Co,Ltd.</h3>
            <p class="mt-lg-4 mt-md-3 mt-2 p-2 pl-4">CCTV & Security Solutions...</p>
         </div>
      </div>
      <div class="carousel-item b4">
         <div class="container banner-info-w3-agile">
            <h3>Intelligence Power Co,Ltd.</h3>
            <p class="mt-lg-4 mt-md-3 mt-2 p-2 pl-4">CCTV & Security Solutions...</p>
         </div>
      </div>
   </div>
   <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
   <span class="carousel-control-prev-icon" aria-hidden="true"></span>
   <span class="sr-only">Previous</span>
   </a>
   <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
   <span class="carousel-control-next-icon" aria-hidden="true"></span>
   <span class="sr-only">Next</span>
   </a>
</div>
</section>
<!-- //banner -->
<!--about-->
<section class="banner-btm-section py-5" id="about">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">Company Brief</h5>
      <div class="srategy-text mb-5">
         <p class="paragraph-agileinfo"><b>Intelligence Power Company was registered in (15-11-2016)Register No.2016/109489069/11.</b>
         The company is dedicated to Security Service especially in the field of CCTV system ,Telecommunication system , Alarm system , --------and intelligent building / Hotel management system. 
         Our Team are well trained and well equipped with the advanced Tooling, Machine and skills. We provide the International quality service to customers. We collaborate with other members to provide the unique advice to our customers.
         </p>
      </div>
      <div class="bnr-btm-main row justify-content-around">
         <div class="col-lg-4 banner-btm-grids">
            <div class="bnr-btm-info bb1 p-41">
               <img src="images/ip-icon1.png">
               <h3 class="subheading-wthree mb-md-4 mb-3">Vision</h3>
               <p class="paragraph-agileinfo text-white">Intelligence Power Co., Ltd.(IP) cooperation principle is to obtain WWW target with
customers , Business partners as well as competitors . We aimed to create a good business environment for growth share and sustainable development ,long term good relationship with everyone no matter he was.</p>
            </div>
         </div>
         <div class="col-lg-4 banner-btm-grids my-lg-0 my-4">
            <div class="bnr-btm-info bb2 p-41">
               <img src="images/ip-icon2.png">
               <h3 class="subheading-wthree mb-md-4 mb-3">Mission</h3>
               <p class="paragraph-agileinfo text-white">It is our mission at Intelligence Power Co., Ltd.(IP) to be “One-stop of M&E service
provider” for your Turnkey project. We have plenty of Famous Branded products resources and competitive installation price for your tailored requirement from the project design to turn-key project with good co-operation.</p>
            </div>
         </div>
         <div class="col-lg-4 banner-btm-grids">
            <div class="bnr-btm-info bb3 p-41">
               <img src="images/ip-icon3.png">
               <h3 class="subheading-wthree mb-md-4 mb-3">Goal</h3>
               <p class="paragraph-agileinfo text-white">Intelligence Power Co., Ltd.(IP) is an accredited installer of many brands with many
years of experience in the electronic security systems industry. We have hired highly qualified technicians to perform our quality installations, with the goal to surpass our customer’s expectations. </p><br>
            </div>
         </div>
      </div>
   </div>
</section>
<!--//about-->
<!-- services -->
<section class="services-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">How we work</h5>
      <div class="services-grids row">
         <div class="col-lg-4 services-info-agileits p-lg-4 p-3 text-left">
            <img src="images/h1.jpg" class="img-fluid mb-3" alt="Responsive image">
            <h3 class="subheading-wthree mb-md-4 mb-3">Team work</h3>
            <ul>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ Our culture of team-work allows us
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ to combine the quality and expertise
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ of our professional staffs to deliver
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ optimum solutions to our clients
               </li>
            </ul>
         </div>
         <div class="col-lg-4 services-info-agileits p-lg-4 p-3 text-center">
            <h3 class="subheading-wthree mb-md-4 mb-3">Excellence</h3>
            <ul>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ Our commitment   
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ to professional excellence ensures
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ that our clients receive
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  ▪️ the highest quality service. 
               </li>
            </ul>
            <img src="images/h2.jpg" class="img-fluid mt-3" alt="Responsive image">
         </div>
         <div class="col-lg-4 services-info-agileits p-lg-4 p-3 text-right">
            <img src="images/h3.jpg" class="img-fluid mb-3" alt="Responsive image">
            <h3 class="subheading-wthree mb-md-4 mb-3">Innovation & Communication</h3>
            <ul>
               <li class="mt-sm-2 mt-1 p-2">
                  We thrive on creativity and ingenuity.   ▪️
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                   In today’s fast paced Technological climate
, innovative ideas , concepts to the continued success and growth of a company. ▪️
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  We ensure that we communicate openly , accurately ▪️
               </li>
               <li class="mt-sm-2 mt-1 p-2">
                  in a timely manner with our stakeholders. ▪️
               </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<!-- //services -->
<!-- News -->
<section class="News-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">Our Activities</h5>
      <div class="row">
         <div class="col-md-6 w3_agile_services_grid mt-md-5 mt-4">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/certificate.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">14 November</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="">Certificate Of Authorization</a>
            </h4>
            <p class="paragraph-agileinfo">We hereby authorize Intelligence Power Co,Ltd. as authorized distributor to sell all the audio products under the brand of PEAa.
            </p>
            <a class="btn btn-primary mt-3" href="https://www.facebook.com/ipcctvinstaller/" target="blank_" role="button">Read More</a>
         </div>
         <div class="col-md-6 w3_agile_services_grid mt-md-5 mt-4">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/certi.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">14 November</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="">Certificate Of Authorization</a>
            </h4>
            <p class="paragraph-agileinfo">We hereby authorize Intelligence Power Co,Ltd. as authorized distributor to sell all the audio products under the brand of PEAa.
            </p>
            <a class="btn btn-primary mt-3" href="https://www.facebook.com/ipcctvinstaller/" target="blank_" role="button">Read More</a>
         </div>
      </div>
   </div>
</section>
<!-- //News -->
<!--subscribe-->
<section class="subscribe-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3 text-white">Our Partners</h5>
      <div class="bnr-btm-main row justify-content-around">
         <div class="col-lg-2 banner-btm-grids my-lg-0 my-2" >
            <img src="images/ip-partner1.png" class="logo-icon"> &nbsp;  
            <img src="images/ip-partner2.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner3.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner4.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner5.png" class="logo-icon"> &nbsp;
         </div>
         <div class="col-lg-2 banner-btm-grids my-lg-0 my-2">
            <img src="images/ip-partner6.png" class="logo-icon"> &nbsp;  
            <img src="images/ip-partner7.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner8.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner9.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner10.png" class="logo-icon"> &nbsp;
         </div>
         <div class="col-lg-2 banner-btm-grids my-lg-0 my-2">
            <img src="images/ip-partner11.png" class="logo-icon"> &nbsp;  
            <img src="images/ip-partner12.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner13.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner14.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner15.png" class="logo-icon"> &nbsp;
         </div>
         <div class="col-lg-2 banner-btm-grids my-lg-0 my-2">
            <img src="images/ip-partner16.png" class="logo-icon"> &nbsp;  
            <img src="images/ip-partner17.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner18.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner19.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner20.png" class="logo-icon"> &nbsp;
         </div>
         <div class="col-lg-2 banner-btm-grids my-lg-0 my-2">
            <img src="images/ip-partner21.png" class="logo-icon"> &nbsp;  
            <img src="images/ip-partner22.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner23.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner24.png" class="logo-icon"> &nbsp;
            <img src="images/ip-partner25.png" class="logo-icon"> &nbsp;
         </div>
         <!-- <div class="col-lg-2 banner-btm-grids my-lg-0 my-2">
            <img src="images/checkpoint.png" class="logo-icon"> &nbsp;
            <img src="images/checkpoint.png" class="logo-icon"> &nbsp;
         </div> -->
      </div>
   </div>
</section>
<!--//subscribe-->
@include('footer')
<!-- Login Modal -->
<!-- <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
   <div class="modal-dialog" role="document">
   	<div class="modal-content">
   		<div class="modal-header">
   			<h5 class="modal-title" id="exampleModalLabel1">Login</h5>
   			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
   				<span aria-hidden="true">&times;</span>
   			</button>
   		</div>
   		<div class="modal-body">
   			<div class="login p-md-5 p-4">
   				<form action="#" method="post">
   					<div class="form-group">
   						<label>Email address</label>
   						<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required="">
   						<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
   					</div>
   					<div class="form-group">
   						<label>Password</label>
   						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="" required="">
   					</div>
   					<div class="form-check mb-2">
   						<input type="checkbox" class="form-check-input" id="exampleCheck1">
   						<label class="form-check-label" for="exampleCheck1">Check me out</label>
   					</div>
   					<button type="submit" class="btn btn-primary submit mb-4">Login</button>
   					<p>
   						<a href="#" class="modal-btm-text" data-toggle="modal" data-target="#exampleModal2"> Don't have an account?</a>
   					</p>
   				</form>
   			</div>
   
   		</div>
   	</div>
   </div>
   </div> -->
<!--// Login Modal -->
<!-- Required common Js -->
<script src='js/jquery-2.2.3.min.js'></script>
<!-- //Required common Js -->
<!-- stats -->
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.countup.js"></script>
<script>
   $('.counter').countUp();
</script>
<!-- //stats -->
<!-- flexSlider -->
<script defer src="js/jquery.flexslider.js"></script>
<script>
   $(window).load(function () {
   	$('.flexslider').flexslider({
   		animation: "slide",
   		start: function (slider) {
   			$('body').removeClass('loading');
   		}
   	});
   });
</script>
<!-- //flexSlider -->
<!-- password-script -->
<script>
   window.onload = function () {
   	document.getElementById("password1").onchange = validatePassword;
   	document.getElementById("password2").onchange = validatePassword;
   }
   
   function validatePassword() {
   	var pass2 = document.getElementById("password2").value;
   	var pass1 = document.getElementById("password1").value;
   	if (pass1 != pass2)
   		document.getElementById("password2").setCustomValidity("Passwords Don't Match");
   	else
   		document.getElementById("password2").setCustomValidity('');
   	//empty string means no validation error
   }
</script>
<!-- //password-script -->
<!-- start-smoth-scrolling -->
<script src="js/move-top.js"></script>
<script src="js/easing.js"></script>
<script>
   jQuery(document).ready(function ($) {
   	$(".scroll").click(function (event) {
   		event.preventDefault();
   		$('html,body').animate({
   			scrollTop: $(this.hash).offset().top
   		}, 1000);
   	});
   });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script>
   $(document).ready(function () {
   	/*
   		var defaults = {
   		containerID: 'toTop', // fading element id
   		containerHoverID: 'toTopHover', // fading element hover id
   		scrollSpeed: 1200,
   		easingType: 'linear'
   		};
   	*/
   
   	$().UItoTop({
   		easingType: 'easeOutQuart'
   	});
   
   });
</script>
<!-- //here ends scrolling icon -->
<!-- Js for bootstrap working-->
<script src="js/bootstrap.min.js"></script>
<!-- //Js for bootstrap working -->
</body>
</html>

