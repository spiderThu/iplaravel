<!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->
<!DOCTYPE HTML>
<html>
   <head>
      <title>Intelligence Power Company Limited</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="ip,intellpower,Intelligence,cctv,ipcctv,ippower,network,IntelligencePower" />
      <LINK rel="SHORTCUT ICON" href="images/dashboard/ipshortcut.png">
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
      <!-- Bootstrap Core CSS -->
      <link href="css/dashboard/bootstrap.css" rel='stylesheet' type='text/css' />
      <!-- Custom CSS -->
      <link href="css/dashboard/style.css" rel='stylesheet' type='text/css' />
      <!-- font-awesome icons CSS -->
      <link href="css/dashboard/font-awesome.css" rel="stylesheet">
      <!-- //font-awesome icons CSS -->
      <!-- side nav css file -->
      <link href='css/dashboard/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
      <!-- side nav css file -->
      <!-- js-->
      <script src="js/dashboard/jquery-1.11.1.min.js"></script>
      <script src="js/dashboard/modernizr.custom.js"></script>
      <!--webfonts-->
      <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
      <!--//webfonts--> 
      <!-- Metis Menu -->
      <script src="js/dashboard/metisMenu.min.js"></script>
      <script src="js/dashboard/custom.js"></script>
      <link href="css/dashboard/custom.css" rel="stylesheet">
      <!--//Metis Menu -->
   </head>
   <body class="cbp-spmenu-push">
      <div class="main-content">
         <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
            <!--left-fixed -navigation-->
            <aside class="sidebar-left">
               <nav class="navbar navbar-inverse">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                     <h1><a class="navbar-brand" href="{{ route('home') }}"><img src="images/dashboard/ipshortcut.png" style="width: 30px;"/><span class="dashboard_text">Design dashboard</span></a></h1>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                           <a href="{{ route('home') }}">
                           💻 <span>Dashboard</span>
                           </a>
                        </li>
                        <li class="treeview">
                           <a href="{{ route('service-list') }}">
                           💻
                           <span>Service Management</span>
                           </a>
                           <ul class="treeview-menu">
                              <li><a href="{{ route('service-list') }}">➤ Service List</a></li>
                              <li><a href="{{ route('addnew-service') }}">➤ Add New Service</a></li>
                           </ul>
                        </li>
                        <li class="treeview">
                           <a href="{{ route('project-list') }}">
                           💻
                           <span>Project Management</span>
                           </a>
                           <ul class="treeview-menu">
                              <li><a href="{{ route('project-list') }}">➤ Project List</a></li>
                              <li><a href="{{ route('addnew-project') }}">➤ Add New Project</a></li>
                           </ul>
                        </li>
                        <li class="treeview">
                           <a href="{{ route('activity-list') }}">
                           💻
                           <span>Activities Management</span>
                           </a>
                           <ul class="treeview-menu">
                              <li><a href="{{ route('activity-list') }}">➤ Activities List</a></li>
                              <li><a href="{{ route('addnew-activity') }}">➤ Add New Activity</a></li>
                           </ul>
                        </li>
                        <li class="treeview">
                           <a href="{{ route('admin-list') }}">
                           💻 <span>Admin Management</span>
                           </a>
                           <ul class="treeview-menu">
                              <li><a href="{{ route('admin-list') }}">➤ Admin List</a></li>
                              <li><a href="{{ route('admin-register') }}">➤ Register</a></li>
                           </ul>
                        </li>
                     </ul>
                  </div>
                  <!-- /.navbar-collapse -->
               </nav>
            </aside>
         </div>
         <!--left-fixed -navigation-->
         <!-- header-starts -->
         <div class="sticky-header header-section ">
            <div class="header-left">
               <!--toggle button start-->
               <button id="showLeftPush">⇱</button>
               <!--toggle button end-->
               <div class="clearfix"> </div>
            </div>
            <div class="header-right">
               <div class="profile_details">
                  <ul>
                     <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                           <div class="profile_img">
                              <span class="prfil-img"><img src="images/dashboard/ipshortcut.png" alt="" style="width: 30px;"> </span> 
                              <div class="user-name">
                                 <p>Admin Name</p>
                                 ⇓
                                 <span>{{ Auth::user()->name }}</span>
                              </div>
                              <!-- <i class="fa fa-angle-down lnr"></i>
                              <i class="fa fa-angle-up lnr"></i> -->
                              <div class="clearfix"></div>
                           </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                           <li> <a href="{{ route('logout') }}">➲ Logout</a> </li>
                        </ul>
                     </li>
                  </ul>
               </div>
               <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
         </div>
         <!-- //header-ends -->
         <!-- main content start-->
         <div id="page-wrapper">
            <div class="main-page">
               <div class="tables">
                  <h2 class="title1">Activity Edit</h2>
                  <div class=" form-grids row form-grids-right">
                     <div class="widget-shadow " data-example-id="basic-forms">
                        <div class="form-body">
                           <form class="form-horizontal">
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-2 control-label"> Activity Name</label> 
                                 <div class="col-sm-9"> <input type="text" class="form-control" id="inputEmail3" placeholder="Activity Name"> </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-2 control-label"> Activity Date </label> 
                                 <div class="col-sm-9"> <input type="date" class="form-control" id="inputEmail3"> </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-2 control-label">Description</label> 
                                 <div class="col-sm-9"><textarea class="form-controll" rows="4" cols="50"></textarea> </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputPassword3" class="col-sm-2 control-label">Activity Image</label> 
                                 <div class="col-sm-9">
                                    <input type="file" id="exampleInputFile"> 
                                    <p class="help-block">Example block-level help text here.</p>
                                    <img class="imagesize" src="images/dashboard/ipshortcut.png"> 
                                 </div>
                              </div>
                              <div class="form-group">
                              </div>
                              <div class="col-sm-offset-2"> <button type="submit" class="btn btn-default">Update</button> </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--footer-->
         <div class="footer">
            <p>&copy;2019 Intelligence Power Co,Ltd. All Rights Reserved | Design by <a href="https://ihost.spidernetworkict.com/" target="_blank">iHost</a></p>
         </div>
         <!--//footer-->
      </div>
      <!-- side nav js -->
      <script src='js/dashboard/SidebarNav.min.js' type='text/javascript'></script>
      <script>
         $('.sidebar-menu').SidebarNav()
      </script>
      <!-- //side nav js -->
      <!-- Classie --><!-- for toggle left push menu script -->
      <script src="js/dashboard/classie.js"></script>
      <script>
         var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
           showLeftPush = document.getElementById( 'showLeftPush' ),
           body = document.body;
           
         showLeftPush.onclick = function() {
           classie.toggle( this, 'active' );
           classie.toggle( body, 'cbp-spmenu-push-toright' );
           classie.toggle( menuLeft, 'cbp-spmenu-open' );
           disableOther( 'showLeftPush' );
         };
         
         function disableOther( button ) {
           if( button !== 'showLeftPush' ) {
             classie.toggle( showLeftPush, 'disabled' );
           }
         }
      </script>
      <!-- //Classie --><!-- //for toggle left push menu script -->
      <!--scrolling js-->
      <script src="js/dashboard/jquery.nicescroll.js"></script>
      <script src="js/dashboard/scripts.js"></script>
      <!--//scrolling js-->
      <!-- Bootstrap Core JavaScript -->
      <script src="js/dashboard/bootstrap.js"> </script>
   </body>
</html>

