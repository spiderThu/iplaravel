@include('header')
<style>
   .ser td {
      text-shadow: 2px 2px rgba(0,0,0,0.1);
    font-size: 19px;
    font-weight: 400;
    padding: 2px 4px 9px 55px;
   }
</style>
<link href="css/valuecust.css" rel="stylesheet" type="text/css" media="all" />
<!-- breadcrumb -->
<nav aria-label="breadcrumb">
   <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{{ route('index') }}">Home</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Services</li>
   </ol>
</nav>
<!-- //breadcrumb -->
<!-- News -->
<section class="News-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">Our Services</h5>
      <p class="paragraph-agileinfo">
         Intelligence Power Co., Ltd.(IP) offers state-of-the-art security systems services to a vast range of
         industries, including:<br>
         <span style="color: #18e2fa;"><b>▪️ Hotel &nbsp;&nbsp;
         ▪️ Guest House &nbsp;&nbsp;
         ▪️ Retail &nbsp;&nbsp;
         ▪️ Restaurants &nbsp;&nbsp;
         ▪️ Residential &nbsp;&nbsp;
         ▪️ Banks &nbsp;&nbsp;
         ▪️ Government departments &nbsp;&nbsp;
         ▪️ Education <br>
         ▪️ Healthcare &nbsp;&nbsp;
         ▪️ Auto and garages &nbsp;&nbsp;
         ▪️ Construction Sites</b></span><br>
         We work hand in hand with our clients to deliver a solution according to their specification and requirements. We also offer a friendly training to our clients after installation. Intelligence Power Co., Ltd.(IP) aims to establish lasting reliable and valued relationship with its
         clients. 
      </p>
      <br>
      <p class="paragraph-agileinfo">Our range of service includes <b>Design, Installation and Repair</b> of:</p>
      <br>
      <table  align="center" class="ser">
         <tr>
            <td>◾ CCTV System</td>
            <td>◾ CATV , DTV , MATV System</td>
         </tr>
         <tr>
            <td>◾ PA / Sound System</td>
            <td>◾ Time Attendance & Finger Print System</td>
         </tr>
         <tr>
            <td>◾ Door Access System</td>
            <td>◾ Car GPS System</td>
         </tr>
         <tr>
            <td>◾ Fire Alarm System</td>
            <td>◾ PABX / IP-PBX System</td>
         </tr>
         <tr>
            <td>◾ Local Network Enterprise Wi-Fi (LAN & Fiber)</td>
            <td>◾ Feasible Survey and Estimation</td>
         </tr>
         <tr>
            <td>◾ Technical / Quality Consultancy and Price Quotation</td>
            <td>◾ One-stop Services</td>
         </tr>
         <tr>
            <td>◾ Skillful & Experience Persons</td>
            <td>◾ Emergency Call and Services</td>
         </tr>
         <tr>
            <td>◾ Can negotiate for Payment Terms</td>
            <td>◾ Installations</td>
         </tr>
         <tr>
            <td>◾ Door To Door System</td>
            <td>◾ Operating & Maintenance (Regular & Subsequent Year Service)</td>
         </tr>
      </table>
   </div>
</section>
<!-- //News -->
<!-- Pricing -->
<section class="pricing-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <!-- <h5 class="main-w3l-title mb-sm-4 mb-3">Our Pricing</h5> -->
      <div class="card-deck text-center row">
         <div class="card box-shadow col-lg-6">
            <div class="card-header">
               <h4 class="py-md-4 py-3">Security System Solutions</h4>
            </div>
            <div class="card-body">
               <!-- <h5 class="card-title pricing-card-title">
                  <span class="align-top">$</span>25
                  <small class="text-muted">/ month</small>
                  </h5> -->
               <ul class="list-unstyled mt-3 mb-4">
                  <li class="py-2 border-bottom">CCTV / CATV / DTV / MATV</li>
                  <li class="py-2 border-bottom">Access Control System & Panorama IP Solution</li>
                  <li class="py-2 border-bottom">Video Conference & Intercom System</li>
                  <li class="py-2 border-bottom">PA / Speaker Announcement System & Hotel Management System</li>
                  <li class="py-2 border-bottom">Enterprise Wi-Fi & POS Software</li>
                  <li class="py-2 border-bottom">Check Gate & Traffic Enforcement System</li>
                  <li class="py-2 border-bottom">Smart Home System & Intelligent Building Solution</li>
                  <li class="py-2 border-bottom">Fire Alarm System & Motion Detector Solution</li>
                  <li class="py-2 border-bottom">Fire Fighting Solution</li>
               </ul>
               <!-- <button type="button" class="btn btn-block btn-outline-primary py-2" data-toggle="modal" data-target="#exampleModal3">Contact Us</button> -->
            </div>
         </div>
         <!--  <div class="card box-shadow col-lg-4 my-lg-0 my-3">
            <div class="card-header">
               <h4 class="py-md-4 py-3">Standard</h4>
            </div>
            <div class="card-body">
               <h5 class="card-title pricing-card-title">
                  <span class="align-top">$</span>59
                  <small class="text-muted">/ month</small>
               </h5>
               <ul class="list-unstyled mt-3 mb-4">
                  <li class="py-2 border-bottom">Advertising</li>
                  <li class="py-2 border-bottom">Branding Services</li>
                  <li class="py-2 border-bottom">Online Marketing</li>
                  <li class="py-2">Creative Marketing</li>
               </ul>
               <button type="button" class="btn btn-block btn-outline-primary py-2" data-toggle="modal" data-target="#exampleModal3">Get Started</button>
            </div>
            </div> -->
         <div class="card box-shadow col-lg-6">
            <div class="card-header">
               <h4 class="py-md-4 py-3">One-stop service</h4>
            </div>
            <div class="card-body">
               <!--  <h5 class="card-title pricing-card-title">
                  <span class="align-top">$</span>90
                  <small class="text-muted">/ month</small>
                  </h5> -->
               <ul class="list-unstyled mt-3 mb-4">
                  <li class="py-2 border-bottom">1-Communications System and Security System Solutions.</li>
                  <li class="py-2 border-bottom">PABX / IP-PBX System (Panasonic Brand)</li>
                  <li class="py-2 border-bottom">VOIP & Nurse Call System / Call Accounting System</li>
                  <li class="py-2 border-bottom">Net-work and Line Diagram Design( LAN) & Cable Installation (Copper/ FOC)</li>
                  <li class="py-2 border-bottom">BTS / IBS Installation &…………….</li>
                  <li class="py-2 border-bottom">Sub Contractor & Net Core (LSP)</li>
                  <li class="py-2 border-bottom">M&E Services / Design, Consultant , Installation , Servicing and Maintenance</li>
                  <li class="py-2 border-bottom">Transmission Device Installation</li>
               </ul>
               <!-- <button type="button" class="btn btn-block btn-outline-primary py-2" data-toggle="modal" data-target="#exampleModal3">Get Started</button> -->
            </div>
         </div>
      </div>
   </div>
</section>
<!-- //pricing -->
<!-- Testimonials -->
<section class="reviews_sec py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">Our Valuable Customers</h5>
      <!-- tabs -->
      <div class="pcss3t pcss3t-effect-scale pcss3t-theme-1">
         <input type="radio" name="pcss3t" checked  id="tab1"class="tab-content-first">
         <label for="tab1">Hotel</label>
         <input type="radio" name="pcss3t" id="tab2" class="tab-content-2">
         <label for="tab2">Condo,Shopping Mall & Construction</label>
         <input type="radio" name="pcss3t" id="tab3" class="tab-content-3">
         <label for="tab3">Hospital</label>
         <input type="radio" name="pcss3t" id="tab4" class="tab-content-4">
         <label for="tab4">Restaurant</label>
         <input type="radio" name="pcss3t" id="tab5" class="tab-content-5">
         <label for="tab5">Monastery</label>
         <input type="radio" name="pcss3t" id="tab6" class="tab-content-last">
         <label for="tab6">City Surveillance</label>
         <ul>
            <li class="tab-content tab-content-first typography">
               <h1>Yangon</h1>
               <p>Sat Sun Hotel ,
Ngu Wah Hotel ,
Green Paradise Hotel,
Top Hotel ,
Aung Zay Ya Guest House,
My Dream Hotel ,
Royal Boss Hotel ,
Hotel Green ,
Silver Green Hotel ,
Academy Guest House ,
Galaxy Hotel ,
Wood Land Hotel ,
GVI Hotel ,
Rainbow Hotel ,
Shwe Sin Guest House ,
Gread Fee Hotel ,
San Yeik Nyein Hotel ,
Inya Lake Hotel </p>
               <h1>Bellin</h1>
               <p>Mingalar Garden Resort</p>
               <h1>Kyaik Hto</h1>
               <p>Kyaik Hto Hotel ,
Mountain Top Hotel ,
The Merit Resort ,
Golden Rock Hotel </p>
               <h1>ChaungThar</h1>
               <p>New Chaung Thar Hotel </p>
               <h1>Bagan</h1>
               <p>Areindmar Hotel </p>
               <h1>Monywar</h1>
               <p>Chindwin Queen Hotel </p>
               <h1>Hintada</h1>
               <p>Gandamar Hotel </p>
               <h1>Sale & Services</h1>
               <p>▪️ IP CCTV
▪️ MATV/DATV
▪️ PABX
▪️ PA
▪️ FireAlarm
▪️ Wi-Fi
▪️ Finger Print</p>
            </li>
            <li class="tab-content tab-content-2 typography">
               <h1>Yangon</h1>
               <p>Khin Sapel Oo Condo ,
My Condo ,
Golden Vallay Cluster House ,
Fit Way Gym ,
A/X Shop Myanmar Plaza ,
EA Shop Sule Lay Square ,
Daw Win
EMPORIO ARMANI
Thiri Zaw Engineering Group ,
Naing Group
Yadanar Myaing Construction</p>
               <h1>Sale & Services</h1>
               <p>▪️ IP CCTV
▪️ Access Control
▪️ CATV
▪️ Video Intercom
▪️ PA
▪️ PABX</p>
            </li>
            <li class="tab-content tab-content-3 typography">
               <h1>Yangon</h1>
               <p>Asia Royal Hospital ,
Aryu Hospital ,
White & Green Hospital</p>
               <h1>Sale & Services</h1>
               <p>▪️ IP CCTV
▪️ Video Intercom
▪️ PABX
</p>
            </li>
            <li class="tab-content tab-content-4 typography">
               <h1>Yangon</h1>
               <p>Thai 47 Restaurant (All Branch ) ,
Chinese 47,
Gloria Jean's Cofee ( All Branch) ,
Shwe Pu Zun ,
Cafe's Dibar ,
Food Harmony ,
Sushitei (All Branch)</p>
               <h1>Sale & Services</h1>
               <p>▪️ IP CCTV
▪️ Finger Print
▪️ FireAlarm
▪️ Wi-Fi
▪️ PA</p>
            </li>
            <li class="tab-content tab-content-5 typography">
               <h1>Yangon</h1>
               <p>San Ya Wai Monastery ,
Kho Nar Yar Monastery ,
Masoeyein Monastery ,
Parami Pyae Monastery </p>
               <h1>Sale & Services</h1>
               <p>▪️ CCTV
▪️ IP CCTV</p>
            </li>
            <li class="tab-content tab-content-last typography">
               <div class="typography">
                  <h1>Yangon</h1>
               <p>San Chaung Township
Thaketa Yan Pyae Road,
Bahan Township
Sayoe Road
Myint Mho Road</p>
               <h1>Sale & Services</h1>
               <p>▪️ IP CCTV
▪️ CCTV</p>
               </div>
            </li>
         </ul>
      </div>
      <!--/ tabs -->
   </div>
</section>
<!-- Testimonials -->
@include('footer')
<!-- Required common Js -->
<script src='js/jquery-2.2.3.min.js'></script>
<!-- //Required common Js -->
<!-- stats -->
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.countup.js"></script>
<script>
   $('.counter').countUp();
</script>
<!-- //stats -->
<!-- flexSlider -->
<script defer src="js/jquery.flexslider.js"></script>
<script>
   $(window).load(function () {
      $('.flexslider').flexslider({
         animation: "slide",
         start: function (slider) {
            $('body').removeClass('loading');
         }
      });
   });
</script>
<!-- //flexSlider -->
<!-- password-script -->
<script>
   window.onload = function () {
      document.getElementById("password1").onchange = validatePassword;
      document.getElementById("password2").onchange = validatePassword;
   }
   
   function validatePassword() {
      var pass2 = document.getElementById("password2").value;
      var pass1 = document.getElementById("password1").value;
      if (pass1 != pass2)
         document.getElementById("password2").setCustomValidity("Passwords Don't Match");
      else
         document.getElementById("password2").setCustomValidity('');
      //empty string means no validation error
   }
</script>
<!-- //password-script -->
<!-- start-smoth-scrolling -->
<script src="js/move-top.js"></script>
<script src="js/easing.js"></script>
<script>
   jQuery(document).ready(function ($) {
      $(".scroll").click(function (event) {
         event.preventDefault();
         $('html,body').animate({
            scrollTop: $(this.hash).offset().top
         }, 1000);
      });
   });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script>
   $(document).ready(function () {
      /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
      */
   
      $().UItoTop({
         easingType: 'easeOutQuart'
      });
   
   });
</script>
<!-- //here ends scrolling icon -->
<!-- Js for bootstrap working-->
<script src="js/bootstrap.min.js"></script>
<!-- //Js for bootstrap working -->
</body>
</html>

