@include('header')
<!-- breadcrumb -->
<nav aria-label="breadcrumb">
   <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{{ route('index') }}">Home</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Our Projects</li>
   </ol>
</nav>
<!-- //breadcrumb -->
<!-- News -->
<section class="News-section py-5">
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3">Our Projects</h5>
      <div class="row">
         <div class="col-md-4 w3_agile_services_grid">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv1.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">19 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">ပါရမီရှိ တိုက်နှစ်လုံး တူရိယာဆိုင်တွင် IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည။</p>
         </div>
         <div class="col-md-4 w3_agile_services_grid my-md-0 my-4">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv2.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">19 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">GWAMBO STUDIOS တွင် Uniview IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
         <div class="col-md-4 w3_agile_services_grid">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv3.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">19 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">Wi-Fi Upgrade</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">Glaxy Hotel တွင် Wi-Fi AP Provisioning Upgrade ပြုလုပ်ခြင်းကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
      </div>
   </div>
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3"> </h5>
      <div class="row">
         <div class="col-md-4 w3_agile_services_grid">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv4.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">19 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">Nyein အဝေးသင် သင်တန်းတွင် HD CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
         <div class="col-md-4 w3_agile_services_grid my-md-0 my-4">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv5.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">9 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">မွန်ပြည်နယ် မုပ္ပလင် မြို့တွင်ရှိသော မရမ်းချောင် စစ်ဆေးရေးဂိတ် တွင် IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
         <div class="col-md-4 w3_agile_services_grid">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv6.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">9 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">ရန်ကုန်မြို့ လှည်းတန်းရှိ Thiri Zaw Construction တွင် Uniview IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
      </div>
   </div>
   <div class="container py-xl-5 py-sm-3">
      <h5 class="main-w3l-title mb-sm-4 mb-3"></h5>
      <div class="row">
         <div class="col-md-4 w3_agile_services_grid">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv7.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">9 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">ရန်ကုန်မြို့ ၂၇ လမ်းအတွင်းရှိ ကြေးအိုးဘုရင် ဆိုင်တွင် IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
         <div class="col-md-4 w3_agile_services_grid my-md-0 my-4">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv8.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">9 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">ရန်ကုန်မြို့ သာကေတ မြို့နယ် အတွင်းရှိ Golden Net အအေးခန်းစက်ရုံတွင် Uniview IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
         <div class="col-md-4 w3_agile_services_grid">
            <div class="agile_services_grid">
               <div class="hover06 column">
                  <div>
                     <a href="#">
                     <img src="images/cctv.jpg" class="img-fluid" alt="Responsive image">
                     </a>
                  </div>
               </div>
               <div class="agile_services_grid_pos">
                  <span class="py-2 px-3">9 Augest</span>
               </div>
            </div>
            <h4 class="mt-3 mb-2">
               <a href="#">CCTV Installation</a>
            </h4>
            <p class="paragraph-agileinfo" style="font-size: 14px;">ရန်ကုန်မြို့ Customer တစ်ဦး၏နေအိမ်တွင် IP CCTV Camera များကို IP အဖွဲ့သားများမှ တပ်ဆင်ပေးခဲ့ပါသည်။</p>
         </div>
      </div>
   </div>
   <br><br>
   <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-center">
         <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">«</span>
            <span class="sr-only">Previous</span>
            </a>
         </li>
         <li class="page-item">
            <a class="page-link" href="#">1</a>
         </li>
         <li class="page-item">
            <a class="page-link" href="#">2</a>
         </li>
         <li class="page-item">
            <a class="page-link" href="#">3</a>
         </li>
         <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">»</span>
            <span class="sr-only">Next</span>
            </a>
         </li>
      </ul>
   </nav>
</section>
<!-- //News -->
@include('footer')
<!-- Require Common Js -->
<script src='js/jquery-2.2.3.min.js'></script>
<!-- //Require Common Js -->
<!-- password-script -->
<script>
   window.onload = function () {
   	document.getElementById("password1").onchange = validatePassword;
   	document.getElementById("password2").onchange = validatePassword;
   }
   
   function validatePassword() {
   	var pass2 = document.getElementById("password2").value;
   	var pass1 = document.getElementById("password1").value;
   	if (pass1 != pass2)
   		document.getElementById("password2").setCustomValidity("Passwords Don't Match");
   	else
   		document.getElementById("password2").setCustomValidity('');
   	//empty string means no validation error
   }
</script>
<!-- //password-script -->
<!-- start-smoth-scrolling -->
<script src="js/move-top.js"></script>
<script src="js/easing.js"></script>
<script>
   jQuery(document).ready(function ($) {
   	$(".scroll").click(function (event) {
   		event.preventDefault();
   		$('html,body').animate({
   			scrollTop: $(this.hash).offset().top
   		}, 1000);
   	});
   });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script>
   $(document).ready(function () {
   	/*
   		var defaults = {
   		containerID: 'toTop', // fading element id
   		containerHoverID: 'toTopHover', // fading element hover id
   		scrollSpeed: 1200,
   		easingType: 'linear'
   		};
   	*/
   
   	$().UItoTop({
   		easingType: 'easeOutQuart'
   	});
   
   });
</script>
<!-- //here ends scrolling icon -->
<!-- Js for bootstrap working-->
<script src="js/bootstrap.min.js"></script>
<!-- //Js for bootstrap working -->
</body>
</html>

